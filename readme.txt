Generic RESTful Webform:

* The goal of this module is to provide a flexible way to map webform submission data to RESTful endpoints, so the end user doesn't have to master TWIG or really know their way around the Webform ecosystem to send forms to their favorite CRM system.

## Version Information

0.1-alpha:
- Built basic structure and code *UNTESTED*