<?php

namespace Drupal\generic_restform\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class emfluenceWebformFieldsMapping
 *
 * @package Drupal\generic_restform\Form
 */

class GenericRestformFieldsMapping extends FormBase {
	
	public function getFormId() {
		
		return 'generic_restform_field_mapping';
	}
	
	public function buildForm(array $form, FormStateInterface $form_state) {

		$webform = \Drupal::entityTypeManager()->getStorage('webform')
			->load(\Drupal::request()->get('webform'));
		$elements = Yaml::decode($webform->get('elements'));
		
		$form_state->setStorage(['webformId' => $webform->id(), 'elements' => $elements]);
		
		$default_values = $this->config('generic_restform.webform_field_mapping.' . $webform->id())->getRawData();
		
		$custom_form_values = $this->config('generic_restform.webform_form_control.' . $webform->id());
		$restform_global_values = $this->config('generic_restform.settings');
		
		// get list of supported fields
		// $restform_manager = \Drupal::service('generic_restform.generic_restform_manager');
		// $tested_fields = $emfluence_manager->get_fields();

		$form['attach_form'] = [
			'#type' => 'checkbox',
			'#title' => $this->t('Send this form to a RESTful service endpoint.'),
			'#default_value' => $custom_form_values->get('attach_form'),
			'#description' => $this->t('Select to send form using Generic Restful Webform.'),
			'#required' => FALSE,
			'#weight' => -20,
		];
		
		$form['by_form_settings'] = [
			'#type' => 'fieldset',
			'#title' => $this->t('Webform Settings'),
			'#states' => [
				'visible' => [
					':input[name="attach_form"]' => ['checked' => TRUE]
				],
			],
		];
		
		$form['by_form_settings']['endpoint_url'] = [
			'#type' => 'url',
			'#title' => $this->t('RESTful Service Endpoint URL'),
			'#default_value' => $custom_form_values->get('endpoint_url'),
			'#description' => $this->t('The full URL (including http:// or https://) of the RESTful endpoint.'),
			'#required' => TRUE,
			'#weight' => -18,
		];
		
		$form['by_form_settings']['header_info'] = [
			'#type' => 'textarea',
			'#title' => $this->t('Headers'),
			'#default_value' => $custom_form_values->get('header_info'),
			'#description' => $this->t('Header info for now...'),
			'#required' => FALSE,
			'#weight' => -15,
		];
		
		$form['by_form_settings']['field_maps'] = [
			'#type' => 'fieldset',
			'#title' => $this->t('Field Mapping'),
		];
		
		$form['by_form_settings']['field_maps']['webform_container'] = [
			'#prefix' => "<div id=form-ajax-wrapper>",
			'#suffix' => "</div>",
		];
		
		foreach ($elements as $key => $element) {
		
			$selected_field = '_none';
			
			if (!empty($default_values[$key])) {
				$selected_field = $default_values[$key]['restform_field'];
			}
			
			$form['by_form_settings']['field_maps']['webform_container'][$key] = [
			  '#type' => 'fieldset',
			  '#title' => isset($element['#title']) ? $element['#title'] : $element['#type'],
			  '#collapsible' => TRUE,
			  '#collapsed' => FALSE,
			];
			
			$form['by_form_settings']['field_maps']['webform_container'][$key][$key . '_restform_field'] = [
			  '#type' => 'textfield',
			  '#title' => t('JSON Field Name'),
			  // '#default_value' => $selected_field,
			  '#description' => $this->t('Enter the name that this field should map to in JSON'),
			];
		
		}
		
		$form['submit'] = [
			'#type' => 'submit',
			'#value' => t('Save'),
		];
		
		return $form;
		
	}
	
	public function submitForm(array &$form, FormStateInterface $form_state) {
	
		
		$values = $form_state->getValues();
		$storage = $form_state->getStorage();
		
		$data = [];
		
		foreach ($storage['elements'] as $key => $element) {
			
			$data[$key] = array(
				'restform_field' => $values[$key . '_restform_field'],
			);
			
		}
		
		$config = \Drupal::configFactory()->getEditable('generic_restform.webform_field_mapping.' . $storage['webformId']);
		$config->setData($data);
		
		$control = \Drupal::configFactory()->getEditable('generic_restform.webform_form_control.' . $storage['webformId']);
		$control->set('attach_form', $values['attach_form'])
			->set('endpoint_url', $values['endpoint_url'])
			->set('header_info', $values['header_info']);
		
		$config->save(TRUE);
		$control->save(TRUE);
		
		\Drupal::messenger()->addMessage(t('Field mappings have been saved.'));
		
	
	}
}